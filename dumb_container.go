package quadtree

type dumb struct {
    elements []*Element
}

func newDumb() *dumb {
    return &dumb{make([]*Element, 0)}
}

func (d *dumb) Insert(e *Element) {
    d.elements = append(d.elements, e)
}

func (d *dumb) FindInRect(rect *Rect) []*Element {
    results := make([]*Element, 0)

    for _, ele := range d.elements {
        if rect.ContainsPoint(ele.X, ele.Y) {
            results = append(results, ele)
        }
    }

    return results
}
