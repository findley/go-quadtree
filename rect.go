package quadtree

type Rect struct {
    X float64
    Y float64
    W float64
    H float64
}

func NewRect(x, y, w, h float64) *Rect {
    return &Rect{x, y, w, h}
}

func (r *Rect) ContainsPoint(x, y float64) bool {
    // Left or right of rect
    if x < r.X || x > r.X + r.H {
        return false
    }

    // Above or below rect
    if y < r.Y || y > r.Y + r.W {
        return false
    }

    return true
}

func (r *Rect) IntersectsRect(rect *Rect) bool {
    // Left of this rect
    if rect.X + rect.W < r.X {
        return false
    }

    // right of this rect
    if rect.X > r.X + r.W {
        return false
    }

    // below this rect
    if rect.Y + rect.H < r.Y {
        return false
    }

    // above this rect
    if rect.Y > r.Y + r.H {
        return false
    }

    return true
}
