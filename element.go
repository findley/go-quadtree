package quadtree

type Element struct {
    X float64
    Y float64
    Value int
}

func NewElement(x, y float64, value int) *Element {
    return &Element{X: x, Y: y, Value: value}
}
