package quadtree

type Settings struct {
    MaxElements int
    MaxDepth int
}

func NewSettings(maxElements, maxDepth int) *Settings {
    return &Settings{
        MaxElements: maxElements,
        MaxDepth: maxDepth,
    }
}
