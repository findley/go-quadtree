package quadtree

import "testing"
import "math/rand"

func BenchmarkDumbInsert100000(b *testing.B) {
    containers := make([]*dumb, b.N)
    for n := 0; n < b.N; n++ {
        containers[n] = newDumb()
    }

    eles := make([]*Element, 100000)
    for i := 0; i < 100000; i++ {
        eles[i] = NewElement(rand.Float64()*10000, rand.Float64()*10000, i)
    }

    b.ResetTimer()

    for n := 0; n < b.N; n++ {
        for i := 0; i < 100000; i++ {
            containers[n].Insert(eles[i])
        }
    }
}

func BenchmarkDumbFind10000(b *testing.B) {
    w := 10000.0
    h := 10000.0
    q := newDumb()
    for i := 0; i < 100000; i++ {
        q.Insert(NewElement(rand.Float64()*w, rand.Float64()*h, i))
    }

    findWidth := w * 0.05
    findHeight := h * 0.05
    searchRects := make([]*Rect, b.N)

    for n := 0; n < b.N; n++ {
        searchRects[n] = NewRect(
            rand.Float64() * 0.95 * w,
            rand.Float64() * 0.95 * h,
            findWidth,
            findHeight,
        )
    }

    b.ResetTimer()

    for n := 0; n < b.N; n++ {
        q.FindInRect(searchRects[n])
    }
}
