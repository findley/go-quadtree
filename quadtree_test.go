package quadtree

import "testing"

func TestFindInRect_Empty(t *testing.T) {
    q := NewQuadtree(NewRect(0, 0, 100, 100), NewSettings(5, 5))

    elements := q.FindInRect(NewRect(0, 0, 20, 20))

    if len(elements) != 0 {
        t.Error("Expected 0 got ", len(elements))
    }
}

func TestFindInRect_MixedQuads(t *testing.T) {
    s := NewSettings(3, 5)
    q := NewQuadtree(NewRect(0, 0, 100, 100), s)
    e1 := NewElement(10, 10, 0)
    e2 := NewElement(11, 11, 1)
    e3 := NewElement(12, 12, 2)
    e4 := NewElement(13, 13, 3)

    q.Insert(e1)
    q.Insert(e2)
    q.Insert(e3)
    q.Insert(e4)

    result := q.FindInRect(NewRect(11.5, 11.5, 2, 2))

    if len(result) != 2 {
        t.Error("Expected 2, got ", len(result))
    }

    if result[0] != e3 && result[0] != e4 {
        t.Error("Expected result[0] to be e3 or e4")
    }

    if result[1] != e3 && result[1] != e4 {
        t.Error("Expected result[1] to be e3 or e4")
    }
}

func TestInsert_EmptyTree(t *testing.T) {
    q := NewQuadtree(NewRect(0, 0, 100, 100), NewSettings(5, 5))
    e := NewElement(10, 10, 1)

    q.Insert(e)

    if q.Size() != 1 {
        t.Error("Expected 1 got ", q.Size())
    }

    if q.elements[0] != e {
        t.Errorf("Expected %p got %p", e, q.elements[0])
    }
}

func TestInsert_Split(t *testing.T) {
    s := NewSettings(3, 5)
    q := NewQuadtree(NewRect(0, 0, 100, 100), s)
    e1 := NewElement(10, 10, 0)
    e2 := NewElement(11, 11, 1)
    e3 := NewElement(12, 12, 2)
    e4 := NewElement(13, 13, 3)

    q.Insert(e1)
    q.Insert(e2)
    q.Insert(e3)
    q.Insert(e4)

    if q.Size() != 4 {
        t.Error("Expected 4 got ", q.Size())
    }

    if q.isLeaf() {
        t.Error("Expected false, got true")
    }

    if q.q3.isLeaf() {
        t.Error("Expected false, got true")
    }

    if q.q3.q3.isLeaf() {
        t.Error("Expected false, got true")
    }

    if !q.q3.q3.q3.isLeaf() {
        t.Error("Expected true, got true")
    }
}

