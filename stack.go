package quadtree

type qtStack struct {
    top *qtElement
}

type qtElement struct {
    val *Quadtree
    next *qtElement
}

func NewStack() *qtStack {
    return &qtStack{}
}

func (s *qtStack) Peek() *Quadtree {
    return s.top.val
}

func (s *qtStack) Pop() *Quadtree {
    q := s.top.val
    s.top = s.top.next
    return q
}

func (s *qtStack) Push(q *Quadtree) {
    e := &qtElement{val: q, next: s.top}
    s.top = e
}

func (s *qtStack) IsEmpty() bool {
    return s.top == nil
}
