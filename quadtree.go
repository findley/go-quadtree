package quadtree

// Quadtree
//       |
//   Q2  |  Q1
//       |
// ------|------
//       |
//   Q3  |  Q4
//       |
type Quadtree struct {
    q1 *Quadtree
    q2 *Quadtree
    q3 *Quadtree
    q4 *Quadtree
    rect *Rect
    elements []*Element
    depth int
    settings *Settings
}

func NewQuadtree(rect *Rect, settings *Settings) *Quadtree {
    return &Quadtree{
        rect: rect,
        elements: make([]*Element, 0),
        depth: 0,
        settings: settings,
    }
}

func (q *Quadtree) Insert(e *Element) {
    if !q.rect.ContainsPoint(e.X, e.Y) {
        return
    }

    leaf := q.selectLeaf(e.X, e.Y)
    leaf.elements = append(leaf.elements, e)

    if leaf.shouldSplit() {
        leaf.split()
    }
}

func (q *Quadtree) FindInRect(rect *Rect) []*Element {
    result := make([]*Element, 0)

    if q.isLeaf() {
        for _, e := range q.elements {
            if rect.ContainsPoint(e.X, e.Y) {
                result = append(result, e)
            }
        }
    } else {
        if rect.IntersectsRect(q.q1.rect) {
            result = append(result, q.q1.FindInRect(rect)...)
        }
        if rect.IntersectsRect(q.q2.rect) {
            result = append(result, q.q2.FindInRect(rect)...)
        }
        if rect.IntersectsRect(q.q3.rect) {
            result = append(result, q.q3.FindInRect(rect)...)
        }
        if rect.IntersectsRect(q.q4.rect) {
            result = append(result, q.q4.FindInRect(rect)...)
        }
    }

    return result
}

func (q *Quadtree) Size() int {
    if q.isLeaf() {
        return len(q.elements)
    }

    return q.q1.Size() + q.q2.Size() + q.q3.Size() + q.q4.Size()
}

func (q *Quadtree) isLeaf() bool {
    return q.elements != nil
}

func (q *Quadtree) shouldSplit() bool {
    return q.isLeaf() &&
        len(q.elements) > q.settings.MaxElements &&
        q.depth < q.settings.MaxDepth
}

func (q *Quadtree) split() {
    hw := q.rect.W / 2.0
    hh := q.rect.H / 2.0
    hx := q.rect.X + hw
    hy := q.rect.Y + hh
    nextDepth := q.depth + 1

    q.q1 = NewQuadtree(NewRect(hx, hy, hw, hh), q.settings)
    q.q1.depth = nextDepth
    q.q2 = NewQuadtree(NewRect(q.rect.X, hy, hw, hh), q.settings)
    q.q2.depth = nextDepth
    q.q3 = NewQuadtree(NewRect(q.rect.X, q.rect.Y, hw, hh), q.settings)
    q.q3.depth = nextDepth
    q.q4 = NewQuadtree(NewRect(hx, q.rect.Y, hw, hh), q.settings)
    q.q4.depth = nextDepth

    elements := q.elements
    q.elements = nil

    for _, e := range elements {
        q.Insert(e)
    }
}

func (q *Quadtree) selectLeaf(x, y float64) *Quadtree {
    stack := NewStack()
    stack.Push(q)

    for !stack.IsEmpty() {
        node := stack.Pop()
        if node.isLeaf() {
            return node
        }

        hx := node.rect.X + node.rect.W / 2.0
        hy := node.rect.Y + node.rect.H / 2.0

        if y > hx {
            if x > hy {
                stack.Push(node.q1)
            } else {
                stack.Push(node.q2)
            }
        } else {
            if x > hy {
                stack.Push(node.q4)
            } else {
                stack.Push(node.q3)
            }
        }
    }

    return nil
}
