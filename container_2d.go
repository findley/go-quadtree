package quadtree

type Container2D interface {
    Insert(e *Element)
    FindInRect(rect *Rect) []*Element
}
